# Essay 3: Algebraic Data Types in Programming

> This essay was published as an article in ["Functional Programming in Practice"][fprog] magazine, in September 2009.

> [Algebraic Data Types][wp_adt] (ADT) is an important programming idiom.
> A theoretical background is given that forms a foundation
> for practical application of ADT in various programming languages.
> Practical aspects of ADTs are discussed using Haskell
> and are also briefly outlined for certain other programming languages.

[fprog]: http://fprog.ru/
[wp_adt]: http://en.wikipedia.org/wiki/Algebraic_data_type
