# Essay 2: Functions and Functional Approach

> This essay was published as an article in
> ["Functional Programming in Practice"][fprog] magazine, in July 2009.

> In terse form the essay describes about the functional approach to
> computational processes (and, in general, to arbitrary processes in real
> world), and about application of this approach in computer science in
> functional programming paradigm. Examples of function implementations are
> given in the programming language [Haskell][haskell].

[fprog]: http://fprog.ru/
[haskell]: http://www.haskell.org/
