# Motivation

Before analyzing the theoretical basis of Algebraic Data Types (ADT), it makes sense to compare their implementation in different programming languages. We are going to examine a programming language where ADTs are not explicitly implemented (for example, C) and another programming language, where ADTs are "natural" constructs (Haskell has the most advanced implementation of ADT).

We will try to implement a data type that represents a [binary tree][btree] such that the programmer has better access and more control over the internal data structure. This implementation should allow to validate certain attributes, for example. All type requirements will be analyzed later in detail.

Suppose we have a simple structure that defines a binary tree (values in such a tree can only be stored in nodes; and leaves are nodes with both subtrees empty):

    :::c
    struct Tree {
        int value;
        struct Tree *l;
        struct Tree *r;
    };

This particular definition does not fully match our needs, since in order to access it's elements or perform different integrity and consistency checks one would require [additional functions][accessors]. Can this definition be changed so that translator would perform all the "dirty work" of implementing helper function for us?

The `Tree` structure can be re-defined in the following way (this is not fully correct, since in such a case only leaves would be able to store values, however that's more than enough for the purpose of this essay):

    :::c
    struct Tree {
        union {
            int value;
            struct {
                struct Tree *left;
                struct Tree *right;
            } branches;
        } data;

        enum {
            LEAF,
            NODE
        } selector;
    };

The new definition has two interdependent elements: a `data` union and a `selector` enumeration. The former contains tree node data, whereas the latter identifies its type. Should the `data` union value be the `value` field, `selector` must be set to `LEAF`. Respectively, if the first element contains the `branches` structure, that consists of two pointers (referencing left and right subtrees), then second element must be set to `NODE`. Yet again we face a necessity in having external instruments, which would look after [semantical consistency][cons] of values.

Developer will have to explicitly define functions like these to allow modification of values of the structure above:

    :::c
    void setValue (Tree *t, int v) {
        t->data.value = v;
        t->selector   = LEAF;
    }

    void setBranches (Tree *t, Tree *l, Tree *r) {
        t->data.branches.left  = l;
        t->data.branches.right = r;
        t->selector            = NODE;
    }

Accessor functions would also need to check whether returning type is similar to the one requested. In the end it would require explicit checks both in accessor functions and all places where they are used. In the type above there are only two alternatives, however there might be a dozen or more alternatives, so that the amount of accessor functions would grow proportionally to the number of fields in a structure.

At this point it must be obvious that definition of type `Tree` in a C-like language can be quite bulky, even though it does solve the important problem of allowing an explicit distinction between the two alternatives: (`value`, `LEAF`) and (`branches`, `NODE`). This type allows storing values of different types depending on its purpose in every particular case: integer values for leaves, and a pair of pointers for nodes. In order to tell which type is used in every case there are 'labels' defined in `selector` enumeration. However, there's price of having these additional 'labels' and a number of explicit checks in accessor functions.

Here is the definition of the same data type in Haskell programming language:

    :::haskell
    data Tree = Leaf Int
              | Node Tree Tree

It defines a type that can be represented by two kinds of values (separated by a vertical line character '|'). First kind of value, marked here as `Leaf`, is an integer being stored in a leaf of a binary tree; second one (marked as `Node`) is a tree node, storing references to left and right subtrees.

Here is the same type, only allowing to store values of random types, not only integers:

    :::haskell
    data Tree α = Leaf α
                | Node (Tree α) (Tree α)

Type variable `α` here is a 'placeholder' for any other type (you can even imagine a situation where a [binary tree contains binary trees][hot] in its leaves). In C-like languages (C++, C#, Java, etc.) one can use templates with the same effect, and an interested readers can try to implement such a template themselves, plus a function to control data integrity (after which one can compare the implementations, for example by number of all sorts of brackets used).

In Haskell, as in many other functional programming languages, a definition of an ADT (and the `data` keyword used earlier indeed defines an ADT) embodies all the necessary checks for data integrity and consistency. Besides, such a formal definition allows to automatically generate templates for processing function, so that the developer only needs to 'fill in the blanks' in them to provide semantics (typical functions for typical cases can be generated 100% automatically).

Here is a quick summary of advantages ADTs provide:

1.  ADTs allow the developer to not waste time writing 'boilerplate' code, be it utility functions, data consistency and integrity checks or even data getters and setters.

2.  ADT is a program entity that defines a safe tagged union.

3.  In the end, ADTs allow to define mutually-recursive data types, and as such are a succinct statically safe implementation of linked structures.

In order to understand ADTs traits and advantages in programming following are the theoretical aspects of algebraic data types, after which, in the last two sections, different approaches to their implementation in programming languages will be described. Code snippets are given mainly in Haskell, with short introduction to ADT related syntax parts. For other programming languages, where ADTs are available, only the examples are given.

-----------------

[btree]: .
  For educational purpose, we are going to disregard the fact that this kind of data type is already implemented as a part of standard library of many modern programming languages.

[accessors]: .
e.g. one would need a function to check that `l` and `r` are not nil, and if they are this is correct (nil pointers are possible for leaves only).

[cons]: .
Certainly, this can be implemented as a class, but it won't change much other than place the structure and its related functions under a single entity.

[hot]: .
There is a term for such cases: 'high-order trees'.
