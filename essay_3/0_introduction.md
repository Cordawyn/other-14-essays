# Introduction

In 1903 english mathematician [Bertrand Russell][br] proposed an antinomy within a classical ("naïve") set theory of [Georg Cantor][gc], which demonstrated the contradiction in the Cantor's definition of the set as "a many thought of as a one" [4][cantor] [[10, 27]]. The antinomy reads:

> Let K be a set of all sets which aren't members of themselves. It is impossible to determine whether K can be a member of itself. If it is, then it cannot be a member of K, as this would contradict the definition. If it isn't, then it must be a member of K, which also contradicts the definition.

Whatever answer you pick, you are facing a paradox.

This antinomy (better known as ["Russell's Paradox"][rp]) rendered a solid blow to the basis of mathematics and formal logic and set the mathematicians of that time to seek the solution. A number of methods was proposed, ranging from renouncing the set theory application in mathematics and limiting the use of quantifiers (as seen in the mathematical philosophy of [Intuitionism][intuit], founded by [Luitzen Egbertus Jan Brouwer][brouwer]), to attempting an axiomatic formalization of set theory ([Zermelo–Fraenkel set theory][zermelo], [Von Neumann-Bernays-Gödel set theory][neumann] and some others). Axiomatic set theories enriched with the [axiom of choice][aoc] or similar theories is actually one of the possible bases of modern mathematics.

Austrian philosopher Kurt Gödel later demonstrated that given a complex enough formal system, one can always find an equation which cannot be proven within the given system (First Incompleteness Theorem of Gödel) [[31]]. This theorem limited the search scope of formal systems, by showing to mathematicians and philosophers that complex systems are guaranteed to contain antinomies similar to the one proposed by Russell.

After all, type theory owes its appearance to Russell's Paradox and the research of formal systems that was initiated by it. Together with the axiomatic set theories and intuitionism, type theory has become one of the means to solve the contradictions of the naïve set theory. Type theory is described today as a formal system which complements the naïve set theory [[30]]. Type theory describes the domain and range of the function - that is, sets of elements that can be used as function arguments and function results. General understanding of type theory in computer science is that data has certain type, or belong to a certain set of values [5][cs_types].

The solution of Russell's antinomy was found within the boundaries of type theory and suggested by Russell himself. It was based on the idea that a set (or class) and its elements have different logic types, such that the set type is more general than the type of the elements comprising it. Many mathematicians of that time rejected this solution, however, considering that it had imposed too strict limitations to the mathematical statements.

A certain number of theories, abstractions and idioms have been developed within common type theory. One of those idioms is *Algebraic Data Type* (other prominent theories of discrete mathematics are *Combinatory Logic* and *Theory of Recursive Functions*)[[21, 9, 24]]. Algebraic Data Type (ADT) is the idiom that we are going to examine in this article, because it has great importance in computer science and, in particular, in functional programming. For example, any non-primitive data type is ADT in Haskell.

It should be noted that the types in "pure" mathematics are thought as objects of manipulation. For instance, typed lambda calculus, where type notion is made explicit, types are dealt with as purely syntactic entities. In typed lambda calculus, types are not "sets of values" but "random" symbols and rules for manipulating them.

Consider, for instance, a simple typed lambda calculus devoid of constants like `Int`, `Bool`, etc., where types are expressions made of symbols "→", "∗" and round brackets "(" and ")" using the following simple rules:

  1. ∗ means "type";
  1. if α and β are types, then (α → β) is a type.

In other words, types are regarded as *sequences* like ∗ → (∗ → ∗) → ∗, but not as *sets of values*. Occasionally *type constants* can make their way into that definition, but that does not make much difference.

Bearing that in mind, we are going to refer to "type" when speaking about an implementation for practical applications (even in mathematical sense). It is only the practical application that matters when we move onto computer science and programming.


-----------------

[cs_types]: .
  TODO

[cantor]: .
  Nevertheless, Cantor offered a rather precise definition of a set [[71]]:

  > Unter einer "Menge" verstehen wir jede Zusammenfassung M von bestimmten
  > wohlunterschiedenen Objekten m unserer Anschauung oder unseres Denkens (welche
  > die "Elemente" von M genannt werden) zu einem Ganzen.

  > A set is a gathering together into a whole of definite, distinct objects of
  > our perception and of our thought - which are called elements of the set.

  This definition can be considered as a turning point where mathematics started
  moving from vague speculations towards precise symbolic statements.

[intuit]: http://en.wikipedia.org/wiki/Intuitionism
[brouwer]: http://en.wikipedia.org/wiki/Luitzen_Egbertus_Jan_Brouwer
[zermelo]: http://en.wikipedia.org/wiki/Zermelo%E2%80%93Fraenkel_set_theory
[neumann]: http://en.wikipedia.org/wiki/Von_Neumann-Bernays-G%C3%B6del_set_theory
[rp]: http://en.wikipedia.org/wiki/Russell's_paradox
[gc]: http://en.wikipedia.org/wiki/Georg_Cantor
[br]: http://en.wikipedia.org/wiki/Bertrand_Russell
[aoc]: http://en.wikipedia.org/wiki/Axiom_of_choice
