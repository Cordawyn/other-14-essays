# ADTs in Haskell

Haskell is one of the languages where ADTs are implemented in a way that's closer to the theory of syntax-oriented construction. That's why in order to learn how to use ADTs in programming it makes sense to take a closer look at its syntax. Following are a quick overview of ADT definition with examples, description of a _pattern matching_ mechanism and an ADT classification.

## General definition of an ADT in Haskell

As mentioned earlier, any non-primitive data type in Haskell is [algebraic][alg]. ADTs are defined using a `data` keyword, like this:

    data [context =>] simpletype = constrs [deriving]

where:

*   _context_ - a context of application for type variables (optional);
*   _simpletype_ - name of a type including all the type variables used in all ADT constructors (empty if there are no type variables);
*   _constrs_ - a list of ADT constructors, separated by a `|` character (this symbol corresponds to the discriminated union operation);
*   _deriving_ - list of classes, for which automatic instances of type being defined must be implemented (optional).

Context and instances are terms from the type system with parametric polymorphism and limited type quantification, that's used in Haskell. These terms are outside the scope of this article, and being optional will not be included into the following examples. Later however (in other articles on type theory and functional programming languages type systems) this topic will be covered in detail so the interested reader should "keep them in mind". Moreover _constrs_ component can contain special strictness marks, constructors themselves can be in an infix form (more on this later) and contain named fields (we'll cover it soon, too).


---------
[alg]: .
Primitive types however can also be thought of as a finite or infinite enumeration, which makes it possibe to define them as an ADT. On the other hand, Haskell has a notion of a "functional type", i.e. a type of a function as a program entity (not type of its value but type of a function itself). Examples of functional types are given in theoretical chapter; Haskell uses exactly these types of functions.
