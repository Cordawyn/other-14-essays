# Theoretical foundations

There are two theoretical ways of defining an ADT. One of them uses the set theory approach and corresponding notaion. Using it would help understanding the development of the notion as well as it's path to informatics. The other one uses a specially invented notation for a so called syntax-oriented construction of types and functions (Hoare notation). This notation allows to write readable definitions of types in terms of mathematical type theory. It's interesting to see how this notation is used in programming languages implementation.

## 2.1 Definition of ADT

ADT can be formallly defined as a set of container-like values that can store values of any type (including values of that same type; in this case it would be a recursive ADT). A set of such containers is a data type, i.e. a set of its values.

> Algebraic data type is a tagged union of Cartesian products of sets, or a tagged sum of direct products of sets.


