# Introduction

It is hardly possible to confirm or even to prove any regularity, but it can be
assumed that both procedural and functional methods of computation somehow
connected with human mind peculiarities, which differ from person to person.
Since ancient times these peculiarities led to attempts to classify human
characters using some dual classification scales. As a most trivial example, you
can bring "introversion - extroversion" scale, while reasons that led to
emergence of two computational paradigms are in a different plane than the
example.

Both procedural and functional computational styles have been known in the past,
and now it's impossible to find out which approach appeared first. Sequences of
calculation steps, the procedural style feature, may be considered as a natural
way of human activity expression during its planning. This is due to the fact
that a man has to live in a world, where the inexorable passage of time and the
everyone's resource limitation make people to plan through the steps their
future livelihoods.

However, it can't be said that functional style of computations had been unknown
to humans in one form or another till the theory of computations emergence. Such
techniques as task decomposition in sub-tasks and unsolved problem expression
through solved ones, which are essence of functional approach, also have been
known since ancient times. It should be noted here, that these techniques may be
applied in the procedural approach as a manifestation of functional style in it.
The very approach is the subject of the article, and its clauses are explained
using functional language [Haskell][haskell].

So in what follows, the reader is proposed to get familiar with the ways the
functions are defined, to explore additional interesting methods within the
functional approach, and to delve into the theoretical digression for
understanding of the basis of the functional approach. The author hopes, that
the software developer of any skill level will be able to find something new.

[haskell]: http://www.haskell.org
